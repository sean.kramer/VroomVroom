package connection;

public interface Driver {

	public Output react(Input state);
	
}
