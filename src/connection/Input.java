package connection;
//test
public class Input {

	public static int LEFT = 0;
	public static int LEFT_DIAG = 1;
	public static int FRONT = 2;
	public static int RIGHT_DIAG = 3;
	public static int RIGHT = 4;
	public static int SPEED = 5;
	
	public float left;
	public float leftd;
	public float front;
	public float rightd;
	public float right;
	public float speed;
	
	public float[] toArray() {
		return new float[] {left, leftd, front, rightd, right, speed};
	}
	
}
