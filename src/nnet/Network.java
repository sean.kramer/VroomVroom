package nnet;

import java.util.Random;
import java.util.function.Function;

import org.ejml.simple.SimpleMatrix;

public class Network {
	
	private Function<Double, Double> af;
	public int[] layerSizes;
	public SimpleMatrix[] layers;
	
	public Network(int[] layerSizes, SimpleMatrix[] layers, Function<Double, Double> af) {
		this.layers = layers;
		this.layerSizes = layerSizes;
		this.af = af;
	}
	
	public Network(int[] layerSizes, Function<Double, Double> af) {
		this.layerSizes = layerSizes;
		layers = new SimpleMatrix[layerSizes.length - 1];
		for (int i = 0; i < layers.length; i++) {
			layers[i] = SimpleMatrix.random32(layerSizes[i], layerSizes[i + 1], -1, 1, new Random());
		}
		this.af = af;
	}
	
	public SimpleMatrix run(SimpleMatrix input) {
		for (SimpleMatrix layer : layers) {
			input = input.mult(layer).elementExp().plus(1).elementLog();
		}
		return input.divide(input.elementMaxAbs());
	}
}
