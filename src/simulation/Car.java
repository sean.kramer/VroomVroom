package simulation;

import connection.Driver;
import connection.Input;
import connection.Output;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PVector;

public class Car {
	
	public static final float RADIUS = 20;
	
	public static final float FRICTION = 0.2f;
	
	public static final float DIAMETER = RADIUS * 2;
	public static final float RAD_SQ = RADIUS * RADIUS;
	
	public static final float TOP_SPEED = 350;
	
	public static final float MAX_ACC = 150;
	public static final float MAX_RACC = PApplet.PI * 1.5f;
	
	private Driver driver;
	
	public int color = 0xffff0000;
	public PVector p = new PVector(400, 300);
	
	public PVector v = new PVector();
	
	public float deltaO = 0;
	public float o = PApplet.PI + PApplet.HALF_PI;

	public Car(Driver driver) {
		this.driver = driver;
	}
	
	public void step(Input state) {
		// Get Driver reaction to current world state
		Output output = driver.react(state);
		output.gas = output.gas > 1 ? 1 : output.gas < -1 ? -1 : output.gas;
		output.steer = output.steer > 1 ? 1 : output.steer < -1 ? -1 : output.steer;
		
		// Calculate desired rotational acceleration
		PVector normal = PVector.fromAngle(o);
		if (v.x != 0 || v.y != 0) {
			o += (float) output.steer * MAX_RACC * World.DT * v.mag() / TOP_SPEED;
		}
		
		// Apply turning friction by getting the component of the velocity
		// in the direction horizontal to the bike's orientation
		normal = PVector.fromAngle(o);
		float dot = v.dot(normal.set(-normal.y, normal.x).normalize());
		PVector compOfVelAbsorbed = normal.mult(dot / 30);
		
		// Calculate desired acceleration
		normal = PVector.fromAngle(o);
		float acc = (float) (TOP_SPEED * output.gas - v.mag());
		acc = Math.signum(acc) * Math.min(MAX_ACC * World.DT, Math.abs(acc)); 

		// Forward
		if (v.dot(normal) >= 0 || output.gas > 0) {
			v.sub(compOfVelAbsorbed);
			v.add(normal.setMag(acc));
			// Return some of the lost turning friction energy to the current orientation
			v.add(PVector.fromAngle(o).setMag(compOfVelAbsorbed.mag() / 10));
		}
		// Reverse
		else {}

		

		// Max out speed
		if (v.mag() > TOP_SPEED) { v.setMag(TOP_SPEED); }
		System.out.println(v.mag());
	}

	public void draw(PGraphics g) {
		g.pushMatrix();
		g.translate(p.x, p.y);
		g.rotate(o);
		
		// Wheel
		g.fill(0xff333333);
		g.ellipse(0, 0, DIAMETER * 1.5f, RADIUS);
		
		// Headlights
		g.fill(0xffffff00);
		g.ellipse(RADIUS - RADIUS / 8, -RADIUS / 2, RADIUS / 4, RADIUS / 4);
		g.ellipse(RADIUS - RADIUS / 8, RADIUS / 2, RADIUS / 4, RADIUS / 4);
		
		// Body
		g.fill(color);
		g.ellipse(0, 0, DIAMETER, DIAMETER);
		
		g.popMatrix();
	}
	
}
