package simulation;

import connection.Driver;
import connection.Input;
import connection.Output;

public class KeyboardDriver implements Driver {

	public boolean up, left, right;
	
	@Override
	public Output react(Input state) {
		Output out = new Output();
		out.gas = up ? 1 : 0;
		out.steer = left ? -1 : (right ? 1 : 0);
		return out;
	}
	
}
