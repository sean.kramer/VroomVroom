package simulation;

import processing.core.PGraphics;
import processing.core.PVector;

public class Seg {
	
	public PVector p1;
	public PVector p2;
	public PVector diff;
	public float l2;

	public Seg(float x1, float y1, float x2, float y2) {
		p1 = new PVector(x1, y1);
		p2 = new PVector(x2, y2);
		diff = PVector.sub(p2, p1);
		l2 = diff.magSq();
	}
	
	public Seg(float[] points) {
		this(points[0], points[1], points[2], points[3]);
	}
	
	public PVector closestPoint(PVector point) {
		float arc = Math.max(0, Math.min(1, PVector.sub(point, p1).dot(diff) / l2));
		return PVector.add(p1, PVector.mult(diff, arc));
	}
	
	public void draw(PGraphics g) {
		g.line(p1.x, p1.y, p2.x, p2.y);
	}

	public static void main(String[] args) {
		Seg a = new Seg(0, 0, 0, 1000);
		System.out.println(a.closestPoint(new PVector(20,500)));
	}
	
}
