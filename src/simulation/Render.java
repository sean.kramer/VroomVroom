package simulation;

import processing.core.PApplet;
import processing.core.PVector;

public class Render extends PApplet {

	long prev;
	
	KeyboardDriver driver;
	
	Car testCar;// = new Car();
	World world;

	@Override
	public void keyPressed() { keys(true); }
	@Override
	public void keyReleased() { keys(false); }
	
	private void keys(boolean state) {
		if (key == 'a') { driver.left = state; }
		if (key == 'd') { driver.right = state; }
		if (key == 'w') { driver.up = state; }
	}
	
	
	@Override
	public void setup() {
		driver = new KeyboardDriver();
		testCar = new Car(driver);
		testCar.v = new PVector(0, 0);
		world = new World(Map.L1, testCar);
		ellipseMode(CENTER);
		prev = System.nanoTime();
	}
	
	@Override
	public void settings() {
		size(1000, 750);
	}
	
	@Override
	public void draw() {
		long curr = System.nanoTime();
		double dt = ((curr - prev) / 1e9);
		prev = curr;
		clear();
		world.step(dt);
		world.draw(g);
	}
	
	public static void main(String[] args) {
		PApplet.main("simulation.Render");
	}

}
