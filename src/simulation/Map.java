package simulation;

public class Map {

	public static final float[][] L1 = {
			{0, 0, 0, 750}, //left
			{1000, 0, 1000, 750}, //right
			
			{0, 750, 1000, 750}, //bottom
			{0, 0, 1000, 0}, //top
			
			{250, 500, 750, 500}, //lower
			
			{250, 250, 250, 500}, //first
			{500, 0, 500, 250}, //second
			{750, 250, 750, 500}, //third
	};
	
}
