package simulation;

import java.util.ArrayList;

import connection.Input;
import processing.core.PGraphics;
import processing.core.PVector;

public class World {

	public static final float DT = 1F / 60;

	public ArrayList<Car> cars = new ArrayList<>();
	public ArrayList<Seg> segs = new ArrayList<>();

	public double counter = 0;
	
	public World(float[][] map, Car... cars) {
		for (Car car : cars) { this.cars.add(car); }
		for (float[] seg : map) { segs.add(new Seg(seg)); }
	}
	
	public void step(double dt) {
		counter += dt;
		while (counter > DT) {
			step();
			counter -= DT;
		}
	}
	
	public void step() {
		for (Car car : cars) {
			Input state = new Input();
			state.speed = car.v.magSq();
			car.step(state);
			
			// Resolve Collisions
			for (Seg seg : segs) {
				PVector closest = seg.closestPoint(car.p);
				float pen = Car.RADIUS - closest.dist(car.p);
				if (pen <= 0) { continue; }
				
				// Resolve collision
				PVector normal = PVector.sub(car.p, closest).normalize();
				float dot = car.v.dot(normal);
				
				// Do not resolve if separating or glancing blow
				if (dot >= 0) { continue;}
				
				// Apply an inelastic collision
				PVector compOfVelAbsorbed = normal.mult(dot);
				car.v.sub(compOfVelAbsorbed);
			}
			
			// Update position
			car.p.add(PVector.mult(car.v, DT));
		}
	}
	
	public void draw(PGraphics g) {
		g.stroke(0xffffffff);
		for (Seg seg : segs) { seg.draw(g); }
		g.noStroke();
		for (Car car : cars) { car.draw(g); }
	}
	
}
